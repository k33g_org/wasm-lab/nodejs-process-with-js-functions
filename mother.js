
const fork = require('child_process').fork
childProcess01 = fork("./child-process.js")
childProcess02 = fork("./child-process.js")


// TODO: send "init" with the name of the function


childProcess01.send({
  text: "🤖> Hello from Mum",
  cmd: "load-function",
  functionName: "func01"
})

childProcess02.send({
  text: "🤖> Hello from Mum",
  cmd: "load-function",
  functionName: "func02"
})

// why once (si je n'attends qu'une seule réponse ?)
// je n'écoute qu'une seule fois
childProcess01.once("message", (message) => {
  console.log("🤖> Received by Mum", message)
})

// why once
childProcess02.once("message", (message) => {
  console.log("🤖> Received by Mum", message)
})


childProcess01.send({
  cmd: "exec-function"
})

childProcess02.send({
  cmd: "exec-function"
})


// why once (si je n'attends qu'une seule réponse ?)
childProcess01.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})

// why once
childProcess02.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})


